<?php
declare(strict_types=1);
require_once "./vendor/autoload.php";
/**
 * 使用示例
 */
$password = 'adaw44sfsfsfe';
$verifier = new \Zlf\PasswordVerifier\PasswordVerifier([]);
if ($verifier->verifier('ada5232w4')) {
    print_r(['密码验证通过', $password]);
} else {
    print_r(['密码验证未通过', $verifier->getError()]);
}
